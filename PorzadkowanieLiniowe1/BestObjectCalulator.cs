﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PorzadkowanieLiniowe1
{
    public static class BestObjectCalulator
    {
        public static string GetBestObjects(DataTable dataTable)
        {
            Dictionary<string, int> dict = CalculateTop3(dataTable);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 3; i++)
                sb.AppendLine((i + 1) + ": " + dict.Keys.ElementAt(i));
            return sb.ToString();
        }

         private static Dictionary<string,int> CalculateTop3(DataTable dataTable)
        {
            int[] sums = new int[dataTable.Rows.Count];
            Dictionary<string, int> dict = new Dictionary<string, int>();

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                for (int j = 2; j < dataTable.Columns.Count; j = j + 2)
                {
                    sums[i] += int.Parse(dataTable.Rows[i][j].ToString());
                }
            }
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                dict.Add(dataTable.Rows[i][0].ToString(), sums[i]);
            }

            dict = dict.OrderBy(x => x.Value).ToDictionary(pair => pair.Key, pair => pair.Value);
            return dict;
        }

    }
}
