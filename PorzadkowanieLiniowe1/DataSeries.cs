﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Text;

namespace PorzadkowanieLiniowe
{
    public class DataSeries :ICloneable
    {
        

        public List<Object> ObjectList { get; set; }
        public DataSeries()
        {
            ObjectList = new List<Object>();
        }

        public void GenerateDataSeries(DataTable dt)
        {
            double weight = (1/(double)(dt.Columns.Count-1));
            for(int i = 0; i < dt.Rows.Count; i++)
            {
                ObjectList.Add(new Object((string)dt.Rows[i][0]));
                for (int j = 1; j < dt.Columns.Count; j++)
                {
                    double temp;
                    double.TryParse(dt.Rows[i][j].ToString(), NumberStyles.Any, CultureInfo.InvariantCulture, out temp);
                    ObjectList[i].VariableList.Add(new Variable(dt.Columns[j].ColumnName, temp, weight));
                }
              
            }
        }

        public object Clone()
        {
            var clone = new DataSeries();
            foreach(Object ob in ObjectList)
            {
                clone.ObjectList.Add((Object)ob.Clone());
            }
            return clone;
        }
    }
}
