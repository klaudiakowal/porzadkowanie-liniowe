﻿using System.Data;
using System.Globalization;
using System.IO;
using System.Text;

namespace PorzadkowanieLiniowe
{
    public static class DataFromCSVFile
    {
        public static DataTable ImportDataFromCSVFile(string path)
        {
            DataTable dt = new DataTable();
            char[] separator = {',', ';'};
            using (StreamReader sr = new StreamReader(path,Encoding.UTF8,true))
            {
                string[] headers = sr.ReadLine().Split(separator);
                foreach (string header in headers)
                {
                    dt.Columns.Add(header);
                }
                while (!sr.EndOfStream)
                {
                    string[] rows = sr.ReadLine().Split(separator);

                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }
                    dt.Rows.Add(dr);
                }

            }

            for (int i = 1; i < dt.Rows.Count; i++)
            {
                for (int j = 1; j < dt.Columns.Count; j++)
                {
                    double temp;
                    double.TryParse(dt.Rows[i][j].ToString(), NumberStyles.Any, CultureInfo.InvariantCulture, out temp);
                    dt.Rows[i][j] = temp;
                }
            }

            return dt;
        }

        public static double[,] ConvertDataTableToMatrix(DataTable dt)
        { 
            double[,] matrix = new double[dt.Rows.Count,dt.Columns.Count-1];
           for (int i=0;i<dt.Rows.Count;i++)
          {
                for(int j=1;j<dt.Columns.Count;j++)
                {
                    double temp;
                    double.TryParse(dt.Rows[i][j].ToString(), NumberStyles.Any, CultureInfo.InvariantCulture, out temp);
                    matrix[i,j - 1] = temp;
                }
            }

          return matrix;
        }
    }
}
