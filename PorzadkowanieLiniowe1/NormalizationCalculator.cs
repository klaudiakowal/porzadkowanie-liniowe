﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace PorzadkowanieLiniowe
{
    public class NormalizationCalculator
    {
        public static double[,] NormalizeForTopsis(DataSeries ds)
        {

            int rowCount = ds.ObjectList.Count;
            int columnCount = ds.ObjectList[0].VariableList.Count;
            double[,] result=new double[rowCount,columnCount];

            
            for(int j=0; j<columnCount; j++)
            {
                double sum = 0;
                for (int k = 0; k < rowCount; k++)
                {
                    sum = sum + ds.ObjectList[k].VariableList[j].VariableValue * ds.ObjectList[k].VariableList[j].VariableValue;
                }
                for (int i=0; i<rowCount; i++)
                {
                    
                    result[i, j] = ds.ObjectList[0].VariableList[j].Weight * ds.ObjectList[i].VariableList[j].VariableValue / Math.Sqrt(sum);
                }
            }

            return result;
        }

        public static double[,] Standarize(DataSeries ds)
        {

            int rowCount = ds.ObjectList.Count;
            int columnCount = ds.ObjectList[0].VariableList.Count;
            double[,] result = new double[rowCount, columnCount];

            for (int j = 0; j < columnCount; j++)
            {
                for (int i = 0; i < rowCount; i++)
                {
                    result[i, j] = (ds.ObjectList[0].VariableList[j].Weight) *((1 / CalculateStandardDeviationForVariable(ds, j)) * ds.ObjectList[i].VariableList[j].VariableValue - (CalculateMeanForVariable(ds, j) / CalculateStandardDeviationForVariable(ds, j)));
                }
            }

            return result;
        }
        public static double[,] Unitarize(DataSeries ds)
        {
            int rowCount = ds.ObjectList.Count;
            int columnCount = ds.ObjectList[0].VariableList.Count;
            double[,] result = new double[rowCount, columnCount];

            for (int j = 0; j < columnCount; j++)
            {
                for (int i = 0; i < rowCount; i++)
                {
                    result[i, j] = (ds.ObjectList[0].VariableList[j].Weight) * ((1 /CalculateGapForVariable(ds,j)) * ds.ObjectList[i].VariableList[j].VariableValue - (CalculateMeanForVariable(ds, j) / CalculateGapForVariable(ds,j)));
                }
            }

            return result;
        }

        public static double[,] StandarizePositional(DataSeries ds)
        {
            int rowCount = ds.ObjectList.Count;
            int columnCount = ds.ObjectList[0].VariableList.Count;
            double[,] result = new double[rowCount, columnCount];

            for (int j = 0; j < columnCount; j++)
            {
                for (int i = 0; i < rowCount; i++)
                {
                    double tp = CalculateMedianDeviation(ds, j);
                    result[i, j] = (ds.ObjectList[0].VariableList[j].Weight) * ((1 / tp) * ds.ObjectList[i].VariableList[j].VariableValue - (CalculateMedian(ds, j) / tp));
                }
            }

            return result;
        }

        public static double CalculateMeanForVariable(DataSeries ds, int j)
        {
            double result;
            double sum = 0;
            for(int i=0; i<ds.ObjectList.Count; i++)
            {
                sum = sum + ds.ObjectList[i].VariableList[j].VariableValue;
            }
            result = sum / ds.ObjectList.Count;
            return result;
        }

        public static double CalculateStandardDeviationForVariable(DataSeries ds, int j)
        {
            double[] tmp = new double[ds.ObjectList.Count];
            for(int i=0; i<tmp.Length; i++)
            {
                tmp[i] = ds.ObjectList[i].VariableList[j].VariableValue;
            }
            
            return Essy.Math.Statistical.StandardDeviation_S(tmp);
        }
        public static double CalculateGapForVariable(DataSeries ds, int j)
        {
            double[] tmp = new double[ds.ObjectList.Count];
            double max,min;
            for (int i = 0; i < tmp.Length; i++)
            {
                tmp[i] = ds.ObjectList[i].VariableList[j].VariableValue;
            }
            max = min = tmp[0];
            for (int i=1; i<tmp.Length; i++)
            {
                if (tmp[i] > max)
                    max = tmp[i];
                if (tmp[i] < min)
                    min = tmp[i];
            }

            return max-min;
        }

        public static double CalculateMedian(DataSeries ds, int j)
        {
            double[] tmp = new double[ds.ObjectList.Count];
            for (int i = 0; i < tmp.Length; i++)
            {
                tmp[i] = ds.ObjectList[i].VariableList[j].VariableValue;
            }
            return Median(tmp);
        }

        public static double CalculateMedianDeviation(DataSeries ds, int j)
        {
            //med(|X-med(X)|)
            double[] tmp = new double[ds.ObjectList.Count];
            double[] tmp2 = new double[ds.ObjectList.Count];
            double medX = CalculateMedian(ds, j);
            for (int i = 0; i < tmp.Length; i++)
            {
                tmp[i] = ds.ObjectList[i].VariableList[j].VariableValue;
            }
            for (int i = 0; i < tmp.Length; i++)
            {
                tmp2[i] =Math.Abs(tmp[i] - medX);
            }
            double ret= Median(tmp2);
            return ret; 
        }
        public static double Median(double[] tab)
        {
            List<double> tmp = new List<double>();
            for (int i = 0; i < tab.Length; i++)
            {
                tmp.Add(tab[i]);
            }
            
           tmp =  tmp.OrderBy(x=>x).ToList();
            double wynik;
            if (tab.Length%2==0)
                wynik= (tmp.ElementAt(tab.Length/2-2)+ tmp.ElementAt(tab.Length / 2 - 2))/2;
            else wynik= tmp.ElementAt((tab.Length-1) / 2 );
            return wynik;
        }
        public static DataTable GenerateResult(DataTable ds, double[] result,string name)
        {
            DataColumn newColumn = new System.Data.DataColumn(name, typeof(double));
            newColumn.AllowDBNull = true;
            ds.Columns.Add(newColumn);
            for(int i =0;i<result.Length;i++)
            {
                ds.Rows[i][name] = result[i];
            }
            return ds;
        }
    }
}
