﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PorzadkowanieLiniowe
{
    public class Object : ICloneable
    {
        public string Name { get; set; }
        public List<Variable> VariableList { get; set; }
        public Object(string name)
        {
            Name = name;
            VariableList = new List<Variable>();
        }

        public Object()
        {
            Name = " ";
            VariableList = new List<Variable>();
        }

        public object Clone()
        {
            var clone = new Object();
            clone.Name = this.Name;
            foreach(Variable variable in this.VariableList)
            {

                clone.VariableList.Add((Variable)variable.Clone());
            }
            return clone;
        }
    }
}
