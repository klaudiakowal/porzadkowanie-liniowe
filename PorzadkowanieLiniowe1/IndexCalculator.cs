﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PorzadkowanieLiniowe
{
    public class IndexCalculator
    {
        public static double[] RankSumMethod(DataSeries ds)
        {
            int rowCount = ds.ObjectList.Count;
            int columnCount = ds.ObjectList[0].VariableList.Count;
            double[,] ranks = new double[rowCount, columnCount];

            //Sort values to get their ranks
            List<List<double>> sorter = new List<List<double>>();
            for (int i = 0; i < columnCount; i++)
            {
                sorter.Add(new List<double>());
                for (int j = 0; j < rowCount; j++)
                {
                    sorter[i].Add(ds.ObjectList[j].VariableList[i].VariableValue);
                }
                if (ds.ObjectList[0].VariableList[i].IsStimulant)
                {
                    sorter[i] = sorter[i].OrderBy(x => x)
                        .Reverse()
                        .ToList();
                }
                else
                {
                    sorter[i] = sorter[i].OrderBy(x => x).ToList();
                }
            }
            double[] result = new double[ds.ObjectList.Count];

            //Calculate ranks (with tied ranks) and result
            for (int i = 0; i < columnCount; i++)
            {
                for (int j = 0; j < rowCount; j++)
                {
                    double count = sorter[i].Where(x => x.Equals(ds.ObjectList[j].VariableList[i].VariableValue)).Count();
                    double index = sorter[i].IndexOf(ds.ObjectList[j].VariableList[i].VariableValue) + 1;
                    ranks[j, i] = (index+index+count-1)/2 * ds.ObjectList[0].VariableList[i].Weight;
                    result[j] += ranks[j, i];
                }
            }
            return result;
        }
        public static double[] StandarizedSumMethod(DataSeries ds)
        {
            //Standariize values
            var standarized = NormalizationCalculator.Standarize(ds);
            int rowCount = ds.ObjectList.Count;
            int columnCount = ds.ObjectList[0].VariableList.Count;
            double[] result = new double[rowCount];
            //Calculate result
            for (int i = 0; i < rowCount; i++)
            {
                for (int j = 0; j < columnCount; j++)
                {
                    if (!ds.ObjectList[0].VariableList[j].IsStimulant)
                    {
                        standarized[i, j] *= -1;
                    }
                    result[i] += standarized[i, j] * ds.ObjectList[0].VariableList[j].Weight;
                }
            }
            //Standarize result 
            double min= result.Min();
            double[] temp = new double[result.Length];
            for (int i = 0; i < result.Length; i++) temp[i] = result[i] - min;
            double max = temp.Max();
            for (int i = 0; i < result.Length; i++) result[i] = (result[i] - min) / max;
            return result;
        }

        public static double[] HellwigMethod(DataSeries ds, string method)
        {
            double[,] normalized;
            int rowCount = ds.ObjectList.Count;
            int columnCount = ds.ObjectList[0].VariableList.Count;
            switch (method)
            {         
                case "unitarize":
                    normalized = NormalizationCalculator.Unitarize(ds);
                    break;

                case"positional_standarize":
                    normalized = NormalizationCalculator.StandarizePositional(ds);
                    break;

                case "standarize":
                default:
                    normalized = NormalizationCalculator.Standarize(ds);
                    break;
            }
            double[] ideal = new double[columnCount];
            for (int j = 0; j < columnCount; j++)
            {
                double[] tmp = new double[rowCount];
                for (int i = 0; i < tmp.Length; i++)
                {
                    tmp[i] = normalized[i,j];
                }
                if (ds.ObjectList[0].VariableList[j].IsStimulant)
                    ideal[j] = tmp.Max();
                else ideal[j] = tmp.Min();
            }
            double[] result = new double[rowCount];
            for (int i = 0; i < rowCount; i++)
            {
                double sum = 0;
                for (int j = 0; j < columnCount; j++)
                {
                    sum += Math.Pow(normalized[i, j] - ideal[j],2);
                }
                result[i] = Math.Sqrt(sum);
            }
            double d_zero = Essy.Math.Statistical.Average(result)+2*Essy.Math.Statistical.StandardDeviation_S(result);
            for (int i = 0; i < result.Length; i++)
                result[i] = 1 - result[i] / d_zero;
                
            return result;
        }

        public static double[] TopsisMethod(DataSeries ds)
        {
            double[,] normalized;
            int rowCount = ds.ObjectList.Count;
            int columnCount = ds.ObjectList[0].VariableList.Count;
            normalized = NormalizationCalculator.NormalizeForTopsis(ds);
            double[] ideal = new double[columnCount];
            double[] antyIdeal = new double[columnCount];
            for (int j = 0; j < columnCount; j++)
            {
                double[] tmp = new double[rowCount];
                for (int i = 0; i < tmp.Length; i++)
                {
                    tmp[i] = normalized[i, j];
                }
                if (ds.ObjectList[0].VariableList[j].IsStimulant)
                {
                    ideal[j] = tmp.Max();
                    antyIdeal[j] = tmp.Min();
                }
                else
                {
                    ideal[j] = tmp.Min();
                    antyIdeal[j] = tmp.Max();
                }
            }
            double[] distIdeal = new double[rowCount];
            double[] distAntyIdeal = new double[rowCount];
            double[] result = new double[rowCount];
            for (int i = 0; i < rowCount; i++)
            {
                double sumA = 0;
                double sumI = 0;
                for (int j = 0; j < columnCount; j++)
                {
                    sumA += Math.Pow(normalized[i, j] - antyIdeal[j], 2);
                    sumI += Math.Pow(normalized[i, j] - ideal[j], 2);
                }
                distIdeal[i] = Math.Sqrt(sumI);
                distAntyIdeal[i] = Math.Sqrt(sumA);
                result[i] = (distAntyIdeal[i]) / (distAntyIdeal[i] + distIdeal[i]);
            }
             return result;
        }
    }


}
