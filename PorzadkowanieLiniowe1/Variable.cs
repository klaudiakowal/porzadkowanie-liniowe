﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PorzadkowanieLiniowe
{
    public class Variable : ICloneable
    {
        public Variable(string name, double variableValue)
        {
            Name = name;
            VariableValue = variableValue;
        }
        public Variable(string name, double variableValue,double weight)
        {
            Name = name;
            VariableValue = variableValue;
            Weight = weight;
        }

        public string Name { get; set; }
        public double VariableValue { get; set; }
        public bool IsStimulant { get; set; }
        public double Weight { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public override string ToString()
        {
            return Name + VariableValue;
        }
    }
}
