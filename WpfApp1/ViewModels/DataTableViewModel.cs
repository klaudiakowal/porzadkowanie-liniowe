﻿using PorzadkowanieLiniowe;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace WpfApp1.ViewModels
{
   public  class DataTableViewModel : BaseViewModel
    {
        private DataView _dataSource;
        public DataTable _resultGrid;
        public bool isMainGridShown;
        private DataTable dt;
        private string _buttonContent;
        private readonly DelegateCommand _isButtonClicked;
        public ICommand IsButtonClicked => _isButtonClicked;

        public DataTableViewModel(DataTable data)
        {
            _isButtonClicked = new DelegateCommand(OnChangeDataGrid);
            _resultGrid = new DataTable();
            dt = data;
            isMainGridShown = true;
            DataSource = Dt.DefaultView;
            ButtonContent = "Show table with results";
        }

        private void OnChangeDataGrid(object obj)
        {
            ChangeDataGrid();
        }

        private void ChangeDataGrid()
        {
            if (!isMainGridShown)
            {
                ButtonContent = "Show table with results";
                DataSource = Dt.DefaultView;
                isMainGridShown = true;
            }
            else
            {
                ButtonContent = "Show table with data";
                DataSource = ResultGrid.DefaultView;
                isMainGridShown = false;
            }
        }

        public DataTable Dt
        {
            get => dt;
            set
            {
                SetProperty(ref dt, value);
            }
        }

        public DataView DataSource
        {
            get => _dataSource;
            set => SetProperty(ref _dataSource, value);
        }
        public string ButtonContent
        {
            get => _buttonContent;
            set => SetProperty(ref _buttonContent, value);
        }
        public DataTable ResultGrid
        {
            get => _resultGrid;
            set
            {
                SetProperty(ref _resultGrid, value);
                if(!isMainGridShown)
                {
                    DataSource = null;
                    DataSource = value.DefaultView;
                }
                
            }
        }

    }
}
