﻿using PorzadkowanieLiniowe;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace WpfApp1.ViewModels
{
    public class VariablesIsStymuViewModel : BaseViewModel
    {

        private string _textField;
        private ObservableCollection<VariableIsStymulantCheckBoxListViewItem> _variableList;
        private readonly DelegateCommand _unselectAllCommand;
        public ICommand UnselectAllCommand => _unselectAllCommand;

        private readonly DelegateCommand _selectAllCommand;
        public ICommand SelectAllCommand => _selectAllCommand;

        private readonly DelegateCommand _saveChangesCommand;
        public ICommand SaveChangesCommand => _saveChangesCommand;

        private readonly DelegateCommand _selectedCommand;
        public ICommand SelectedCommand => _selectedCommand;
        private string _stymuDesc= "Stimulants are variables in which the higher the values are, the better this variable is.\n" +
                 "Destimulants are variables  in which the lower the values are, the better this variable is.";


        public VariablesIsStymuViewModel(DataSeries dataSeries)
        {
            DataSeries = dataSeries;
            _unselectAllCommand = new DelegateCommand(OnUnselectedAll);
            _selectAllCommand = new DelegateCommand(OnSelectedAll);
            _saveChangesCommand = new DelegateCommand(OnSaveChanges);
            _selectedCommand = new DelegateCommand(onSelected);
            TextField = "Select for each variable whether it is a stimulant or destimulant";
            VariableList = new ObservableCollection<VariableIsStymulantCheckBoxListViewItem>();  
            foreach (var variable in dataSeries.ObjectList[0].VariableList)
            {
                VariableList.Add(new VariableIsStymulantCheckBoxListViewItem(variable.Name, variable.IsStimulant, variable));
            }   
        }

        private void onSelected(object obj)
        {
            VariableIsStymulantCheckBoxListViewItem checkbox = obj as VariableIsStymulantCheckBoxListViewItem;
            if(checkbox!=null)
            checkbox.IsChecked = checkbox.IsChecked? false : true;
        }

        private void OnSaveChanges(object commandParamete)
        {
            for(int i = 0; i<VariableList.Count;i++)
            {
                for(int j = 0;j<DataSeries.ObjectList.Count;j++)
                {
                     DataSeries.ObjectList[j].VariableList[i].IsStimulant = VariableList[i].IsChecked;
                }
            }
        }

        private void OnSelectedAll(object commandParamete)
        {
            foreach (var item in VariableList)
            {
                if(!item.IsChecked)
                item.IsChecked = true;
            }
            
        }

        private void OnUnselectedAll(object commandParamete)
        {
            foreach (var item in VariableList)
            {
                if (item.IsChecked)
                    item.IsChecked = false;
            }
        }

        public string TextField 
        { 
            get => _textField;
            set => SetProperty(ref _textField, value);
        }

        public string StymuDesc
        {
            get => _stymuDesc;
            set
            {
                SetProperty(ref _stymuDesc, value);
                
            }
        }
    public ObservableCollection<VariableIsStymulantCheckBoxListViewItem> VariableList
        {
            get => _variableList;
            set => SetProperty(ref _variableList, value);
        }

        public DataSeries DataSeries { get; set; }
    }
}
