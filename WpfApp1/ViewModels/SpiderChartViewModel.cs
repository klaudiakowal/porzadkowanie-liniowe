﻿using PorzadkowanieLiniowe;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using GUI;
using WpfCharts;
using System.Data;
using Microsoft.Win32;
using System.IO;
using WpfApp1.Views;
using PorzadkowanieLiniowe1;

namespace WpfApp1.ViewModels
{
    public class SpiderChartViewModel : BaseViewModel
    {
        private LinearOrderingMethod _linearOrderingMethod;
        private DataTable _dt;
        private DataTableViewModel _dataTableViewModel;
        FinalWindow finalWindow = new FinalWindow(null);
        private bool _isBestObjectButtonEnabled;
        private bool _isGenResEnabled;
        private readonly Random random = new Random(1234);
        private string[] _axes;
        private int _minimum;
        private int _maximum;
        private int _ticks;
        private int linesCounter = 0;
        private ObservableCollection<ChartLine> _lines;
        public DataSeries DataSeries { get; set; }

        private readonly DelegateCommand _clearChartCommand;
        public ICommand ClearChartCommand => _clearChartCommand;

        private readonly DelegateCommand _calculateBestObjectsCommand;
        public ICommand CalculateBestObjectsCommand => _calculateBestObjectsCommand;

        private readonly DelegateCommand _generateCSVCommand;
        public ICommand GenerateCSVCommand => _generateCSVCommand;

        private readonly DelegateCommand _addLineCommand;
        public ICommand AddLineCommand => _addLineCommand;

        private  string _chosenMethodLabel;

        public SpiderChartViewModel(LinearOrderingMethod linearOrderingMethod, DataSeries dataSeries, DataTableViewModel dataTableViewModel, DataTable dt)
        {
            DataSeries = dataSeries;
            Dt = dt;
            LinearOrderingMethod = linearOrderingMethod;
            DataTableViewModel = dataTableViewModel;
            _addLineCommand = new DelegateCommand(OnLineAdded);
            _clearChartCommand=new DelegateCommand(LinesDeleted);
            _generateCSVCommand = new DelegateCommand(GenerateCSVClicked);
            _calculateBestObjectsCommand = new DelegateCommand(CalculateBestObjects);
            Axes = new string[dataSeries.ObjectList.Count];
            for(int i =0; i< Axes.Length; i++)  
            {
                Axes[i] = dataSeries.ObjectList[i].Name;
            }

            Minimum = 1;
            Maximum = Axes.Length;
            Ticks = Maximum;
            Lines = new ObservableCollection<ChartLine>();
            IsGenResEnabled = true;
            _dataTableViewModel._resultGrid = new DataTable();
            _dataTableViewModel._resultGrid.Columns.Add(new DataColumn(dt.Columns[0].ColumnName));
            foreach(var obj in DataSeries.ObjectList)
            {
                _dataTableViewModel.ResultGrid.Rows.Add(obj.Name);
            }
        }

        private void GenerateCSVClicked(object commandParameter)
        {
            string defaultFileName = "results";

            SaveFileDialog sf = new SaveFileDialog();
            // Feed the dummy name to the save dialog
            sf.FileName = defaultFileName;
            sf.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
            sf.DefaultExt = ".csv";

            if (sf.ShowDialog() == true)
            {
                // Now here's our save folder
                string savePath = Path.GetFullPath(sf.FileName);

                DataToCSVFile.GenerateCSVFile(_dataTableViewModel.ResultGrid, savePath);
            }
            
        }
        public void CalculateBestObjects(object commandParameter)
        {
            var label =  BestObjectCalulator.GetBestObjects(_dataTableViewModel.ResultGrid);


            if (finalWindow.IsLoaded)
            {
                finalWindow.Close();  
            }
            finalWindow = new FinalWindow(label);
            finalWindow.Show();

        }
        private void LinesDeleted(object commandParameter)
        {
            IsGenResEnabled = true;
            IsBestObjectButtonEnabled = false;
            finalWindow.Close();
            for(int i = 0 ; i < Lines.Count; i++)
            {
                _dataTableViewModel.ResultGrid.Columns.Remove(Lines[i].Name);
                _dataTableViewModel.ResultGrid.Columns.Remove("Ranks" + Lines[i].Name);
            }
            Lines.Clear();
            _dataTableViewModel.ResultGrid = _dataTableViewModel.ResultGrid;
            linesCounter = 0;
        }

        private void OnLineAdded(object commandParameter)
        {
                linesCounter++;
                Random r = new Random();
                IsBestObjectButtonEnabled = true;
                double[] results = new double[DataSeries.ObjectList.Count];
                switch (LinearOrderingMethod.LinOrdMethod)
                {
                    case LinearOrderingMethodName.Unknown:

                        break;
                    case LinearOrderingMethodName.Hellwig:
                        switch (LinearOrderingMethod.NormMethod)
                        {
                            case NormalizationMethodName.Unknown:
                                break;
                            case NormalizationMethodName.Standarization:
                                results = IndexCalculator.HellwigMethod(DataSeries, "standarize");
                                break;
                            case NormalizationMethodName.PostitionalStandarization:
                                results = IndexCalculator.HellwigMethod(DataSeries, "positional_standarize");
                                break;
                            case NormalizationMethodName.Unitarization:
                                results = IndexCalculator.HellwigMethod(DataSeries, "unitarize");
                                break;
                        }
                        break;
                    case LinearOrderingMethodName.TOPSIS:
                        results = IndexCalculator.TopsisMethod(DataSeries);
                        break;
                    case LinearOrderingMethodName.Rank:
                        results = IndexCalculator.RankSumMethod(DataSeries);
                        break;
                    case LinearOrderingMethodName.StandarizedSum:
                        results = IndexCalculator.StandarizedSumMethod(DataSeries);
                        break;
                }
                double[] groups;
                if (LinearOrderingMethod.LinOrdMethod != LinearOrderingMethodName.Rank)
                {
                    groups = results.OrderByDescending(x => x).GroupBy(x => x).Select(x => x.Key).ToArray();
                }
                else
                {
                    groups = results.OrderBy(x => x).GroupBy(x => x).Select(x => x.Key).ToArray();
                }

                var ranks = results.Select(x => Array.IndexOf(groups, x) + 1);
                List<double> ranksDouble = new List<double>();
                foreach (int rank in ranks)
                {
                    ranksDouble.Add(rank);
                }

                var line = new ChartLine
                {
                    LineColor = Color.FromArgb((byte)250, (byte)r.Next(256), (byte)r.Next(256), (byte)r.Next(256)),
                    FillColor = Color.FromArgb((byte)100, (byte)r.Next(256), (byte)r.Next(256), (byte)r.Next(256)),
                    LineThickness = 2,
                    PointDataSource = ranksDouble,
                    Name = (Lines.Count + 1) + ": " + LinearOrderingMethod.LinOrdMethod
                };
                Lines.Add(line);
                NormalizationCalculator.GenerateResult(_dataTableViewModel.ResultGrid, results.Select(d => Math.Round(d, 4)).ToArray(), (Lines.Count) + ": " + LinearOrderingMethod.LinOrdMethod);
                _dataTableViewModel.ResultGrid = NormalizationCalculator.GenerateResult(_dataTableViewModel.ResultGrid, ranksDouble.ToArray(), "Ranks" + (Lines.Count) + ": " + LinearOrderingMethod.LinOrdMethod);

            if (linesCounter == 10)
                IsGenResEnabled = false;
            
          
        }

        public List<double> GenerateRandomDataSet(int nmbrOfPoints)
        {
            var pts = new List<double>(nmbrOfPoints);
            for (var i = 0; i < nmbrOfPoints; i++)
            {
                pts.Add(random.NextDouble() * Maximum);
            }
            return pts;
        }
        public string[] Axes {
            get => _axes;
            set => SetProperty(ref _axes, value);

        }

        public int Minimum
        {
            get => _minimum;
            set => SetProperty(ref _minimum, value);
        }

        public int Maximum
        {
            get => _maximum;
            set => SetProperty(ref _maximum, value);

        }

        public int Ticks
        {
            get => _ticks;
            set => SetProperty(ref _ticks, value);

        }
        public ObservableCollection<ChartLine> Lines
        {
            get => _lines;
            set => SetProperty(ref _lines, value);

        }
        public LinearOrderingMethod LinearOrderingMethod
        {
            get => _linearOrderingMethod;
            set
            {
                SetProperty(ref _linearOrderingMethod, value);
                switch (value.LinOrdMethod)
                {
                    case LinearOrderingMethodName.Unknown:
                        ChosenMethodLabel= "Linear Ordering Method is not set";
                        break;
                    case LinearOrderingMethodName.Hellwig:
                        switch (value.NormMethod)
                        {
                            case NormalizationMethodName.Unknown:
                                ChosenMethodLabel = "Normalization Method for Hellwig is not set";
                                break;
                            case NormalizationMethodName.Standarization:
                                ChosenMethodLabel = "Hellwig with Standarization";
                                break;
                            case NormalizationMethodName.PostitionalStandarization:
                                ChosenMethodLabel = "Hellwig with Positional Standarization";
                                break;
                            case NormalizationMethodName.Unitarization:
                                ChosenMethodLabel = "Hellwig with Unitarization";
                                break;
                        }
                        break;
                    case LinearOrderingMethodName.TOPSIS:
                        ChosenMethodLabel = "TOPSIS";
                        break;
                    case LinearOrderingMethodName.Rank:
                        ChosenMethodLabel = "Rank Sum";
                        break;
                    case LinearOrderingMethodName.StandarizedSum:
                        ChosenMethodLabel = "Standarized Sum";
                        break;

                }
            }
        }
        public DataTableViewModel DataTableViewModel
        {
            get => _dataTableViewModel;
            set => SetProperty(ref _dataTableViewModel, value);
        }
        public DataTable Dt
        {
            get => _dt;
            set => SetProperty(ref _dt, value);
        }
        public string ChosenMethodLabel
        {
            get => _chosenMethodLabel;
            set
            {
                SetProperty(ref _chosenMethodLabel, value);
                            }

        }
        public bool IsBestObjectButtonEnabled
        {
            get => _isBestObjectButtonEnabled;
            set
            {
                SetProperty(ref _isBestObjectButtonEnabled, value);
            }

        }
        public bool IsGenResEnabled
        {
            get => _isGenResEnabled;
            set
            {
                SetProperty(ref _isGenResEnabled, value);
            }

        }
    }
}
