﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.ViewModels
{
    public class LinearMethodViewModel : BaseViewModel
    {
        private bool _isHellwigChecked;
        private bool _isTopsisChecked;
        private bool _isRankSumChecked;
        private bool _isStandarizedSumChecked;
        private bool _isStandarizationChecked;
        private bool _isPositionalStandarizationChecked;
        private bool _isUnitarizationChecked;
        private string _methodDesc;
        public SpiderChartViewModel SpiderChartViewModel { get; set; }

        public LinearMethodViewModel(SpiderChartViewModel spiderChartViewModel)
        {
            SpiderChartViewModel = spiderChartViewModel;
            IsTopsisChecked = true;
        }

        public bool IsPositionalStandarizationChecked
        {
            get => _isPositionalStandarizationChecked;
            set
            {
                SetProperty(ref _isPositionalStandarizationChecked, value);
                if (value)
                    SpiderChartViewModel.LinearOrderingMethod = new LinearOrderingMethod(LinearOrderingMethodName.Hellwig, NormalizationMethodName.PostitionalStandarization);
            }
        }
        public bool IsStandarizationChecked
        {
            get => _isStandarizationChecked;
            set
            {
                SetProperty(ref _isStandarizationChecked, value);
                if (value)
                    SpiderChartViewModel.LinearOrderingMethod = new LinearOrderingMethod(LinearOrderingMethodName.Hellwig, NormalizationMethodName.Standarization);
            }
        }
        public bool IsUnitarizationChecked
        {
            get => _isUnitarizationChecked;
            set
            {
                SetProperty(ref _isUnitarizationChecked, value);
                if (value)
                    SpiderChartViewModel.LinearOrderingMethod = new LinearOrderingMethod(LinearOrderingMethodName.Hellwig, NormalizationMethodName.Unitarization);
            }
        }

        public string MethodDesc
        {
            get => _methodDesc;
            set => SetProperty(ref _methodDesc, value);
        }
        public bool IsHellwigChecked
        {
            get => _isHellwigChecked;
            set
            {
                SetProperty(ref _isHellwigChecked, value);
                if (value)
                {
                    MethodDesc = "Hellwig Method\n" + "The first step of the hellwig ordering algorithm is standardization using one of the methods. Weights are applied to the results. Then a pattern is determined for each variable. Having a pattern, the distance of " + "" +
                            "objects from the pattern is calculated. Then the indicator value is normalized. Objects are given ranks. The lower the indicator value, the worse the rank it will receive.\n"
    + "A certain problem associated with the use of the Hellwig method results from the fact that it assumes the stability of the examined features(e.g.being a stimulant), and above all assumes the equal importance of each of the variables that describe the object.";
                    IsStandarizationChecked = true;
                }
            }
        }
        public bool IsTopsisChecked
        {
            get => _isTopsisChecked;
            set
            {
                SetProperty(ref _isTopsisChecked, value);
                if (value)
                {
                    MethodDesc = "TOPSIS method\n" + "It is a method of compensatory aggregation that compares a set of alternatives by " +
                        "identifying weights for each criterion, normalising scores for each criterion and calculating " +
                        "the geometric distance between each alternative and the ideal alternative, which is the best score" +
                        " in each criterion. An assumption of TOPSIS is that the criteria are monotonically increasing or decreasing. " +
                        "Normalisation is usually required as the parameters or criteria are often of incongruous dimensions " +
                        "in multi-criteria problems. Compensatory methods such as TOPSIS allow trade-offs between criteria, where" +
                        " a poor result in one criterion can be negated by a good result in another criterion." +
                        "The idea of the algorithm is similar to the Hellwig method."
    + "A normalized matrix is created.Variable weights are included. The pattern and anti-pattern and the distances of objects from them are determined.Then the ranking coefficient is determined determining the similarity of objects to the ideal solution. At the very end, objects are given ranks.";
                    SpiderChartViewModel.LinearOrderingMethod = new LinearOrderingMethod(LinearOrderingMethodName.TOPSIS, NormalizationMethodName.Unknown);
                }
            }
        }
        public bool IsRankSumChecked
        {
            get => _isRankSumChecked;
            set
            {
                SetProperty(ref _isRankSumChecked, value);
                if (value)
                {
                    MethodDesc = "Rank Sum method\n" + "The method of sum of ranks consists in determining ranks objects due to each of the variables, then determining their sum or averag." +
                            "The algorithm is to rank all objects within each variable. At this point, the weight of each variable is given. Then the systetic meter is calculated - the weighted average of the rank of variables. On this basis, the objects receive final ranks." +
                            "This method does not work best for variables that are set on a range scale.";
                    SpiderChartViewModel.LinearOrderingMethod = new LinearOrderingMethod(LinearOrderingMethodName.Rank, NormalizationMethodName.Unknown);
                }
            }
        }
        public bool IsStandarizedSumChecked
        {
            get => _isStandarizedSumChecked;
            set
            {
                SetProperty(ref _isStandarizedSumChecked, value);
                if (value)
                {
                    MethodDesc = "Standarized Sum method\n" + "A standard technique for standardless ordering is the method of standardized sums.\n" +
                            "The algorithm involves replacing all variables with stimulants to then standardize the variables - this ensures comparability of characteristics." + "" +
                            " The next step is to sum the estimates obtained within the objects. Finally, it standardizes the values obtained.";
                    SpiderChartViewModel.LinearOrderingMethod = new LinearOrderingMethod(LinearOrderingMethodName.StandarizedSum, NormalizationMethodName.Unknown);
                }
            }

        }
    }

    }
