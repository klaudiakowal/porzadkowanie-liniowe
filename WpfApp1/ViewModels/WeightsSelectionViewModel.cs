﻿using GUI;
using PorzadkowanieLiniowe;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace WpfApp1.ViewModels
{
    public class WeightsSelectionViewModel : BaseViewModel
    {
        private readonly DelegateCommand _selectedCommand;
        public ICommand SelectedCommand => _selectedCommand;

        private readonly DelegateCommand _minusCommand;
        public ICommand MinusCommand => _minusCommand;

        private readonly DelegateCommand _plusCommand;
        public ICommand PlusCommand => _plusCommand;

        private readonly DelegateCommand _equalizeAllCommand;
        public ICommand EqualizeAllCommand => _equalizeAllCommand;

        private ObservableCollection<WeightsListViewItem> _variableList;

        private string _selectedVariable;
        private string _variableWeight;
        private string _variableNotSelectedLabel = "Variable not selected";
        private double _sumOfWeights;
        private string _sumOfWeightsLabel;
        public DataSeries DataSeries { get; set; }
        public MainWindow MainWindow = (MainWindow)Application.Current.MainWindow;

        public string SumOfWeightsLabel
        {
            get => _sumOfWeightsLabel;
            set => SetProperty(ref _sumOfWeightsLabel, value);

        }

        public double SumOfWeights
        {
            get => _sumOfWeights;
            set 
            {
                SetProperty(ref _sumOfWeights, value);
                SumOfWeightsLabel = value.ToString("P2", CultureInfo.InvariantCulture);
            }
            
        }
        public string VariableNotSelectedLabel
        {
            get => _variableNotSelectedLabel;
            set => SetProperty(ref _variableNotSelectedLabel, value);
        }

        public ObservableCollection<WeightsListViewItem> VariableList
        {
            get => _variableList;
            set => SetProperty(ref _variableList, value);
        }

        public string SelectedVariable
        {
            get => _selectedVariable;
            set => SetProperty(ref _selectedVariable, value);
        }

        public string VariableWeight
        {
            get => _variableWeight;
            set
            {
                try
                {
                    double temp;
                    WeightsListViewItem item = VariableList.Where(c => c.VariableName == SelectedVariable).First();
                    if (double.TryParse(value,out temp))
                        if (Math.Round(temp + SumOfWeights - item.Variable.Weight,4) <= 1)
                        {
                            {
                               
                                if (temp >= 0 && temp <= 1)
                                {
                                    item.Variable.Weight = temp;
                                    item.VariableWeight = temp.ToString("P2", CultureInfo.InvariantCulture);
                                }
                                else if (temp < 0)
                                {
                                    item.Variable.Weight = 0;
                                    item.VariableWeight = 0.ToString("P2", CultureInfo.InvariantCulture);
                                }
                                else
                                {
                                    item.Variable.Weight = 1;
                                    item.VariableWeight = 1.ToString("P2", CultureInfo.InvariantCulture);
                                }
                                SetProperty(ref _variableWeight, (Math.Round(item.Variable.Weight, 4)).ToString());
                                CalculateSum();
                                if (Math.Round(SumOfWeights, 4) != 1)
                                {
                                    VariableNotSelectedLabel = "Please adjust weights to 100%";
                                    MainWindow.NextViewModelBt.IsEnabled = false;
                                }
                                else
                                {
                                    MainWindow.NextViewModelBt.IsEnabled = true;
                                    VariableNotSelectedLabel = "";
                                }
                            }
                    }
                        
                                      
                }
                catch (NullReferenceException) { }
            }
        }

        public WeightsSelectionViewModel(DataSeries dataSeries)
        {
            _equalizeAllCommand = new DelegateCommand(OnEqualizeAllClicked);
            _minusCommand = new DelegateCommand(OnMinusClicked);
            _plusCommand = new DelegateCommand(OnPlusClicked);
            _selectedCommand = new DelegateCommand(OnSelected);
            DataSeries = dataSeries;
            VariableList = new ObservableCollection<WeightsListViewItem>();
            foreach (var variable in DataSeries.ObjectList[0].VariableList)
            {
                double weight = Math.Round(variable.Weight, 4);
                VariableList.Add(new WeightsListViewItem(variable.Name, weight.ToString("P2", CultureInfo.InvariantCulture), variable));
            }
        }

        private void OnEqualizeAllClicked(object obj)
        {
           foreach(var item in VariableList)
            {
                item.Variable.Weight = 0;
            }
            foreach (var item in VariableList)
            {
                double w = 1 / (double)VariableList.Count;
                item.Variable.Weight = w;
                item.VariableWeight = item.Variable.Weight.ToString("P2", CultureInfo.InvariantCulture);
            }
            CalculateSum();
            MainWindow.NextViewModelBt.IsEnabled = true;
            VariableNotSelectedLabel = "";


        }

        private void OnPlusClicked(object obj)
        {
            double weight;
            if ((SumOfWeights + 0.05) <=1)
            {
                weight = double.Parse(VariableWeight) + 0.05;
                VariableWeight = weight.ToString();
            }
            else
            {
                weight = 1 - SumOfWeights + double.Parse(VariableWeight);
                VariableWeight = weight.ToString();
            }
           
        }

        private void OnMinusClicked(object obj)
        {
                double weight = double.Parse(VariableWeight) - 0.05;
                VariableWeight = weight.ToString();
        }

       

        private void OnSelected(object obj)
        {
            try
            {
                VariableNotSelectedLabel = "";
                WeightsListViewItem item = obj as WeightsListViewItem;
                SelectedVariable = item.Variable.Name;
                VariableWeight = item.Variable.Weight.ToString();
             }
            catch(NullReferenceException){ }
        }

        private void CalculateSum()
        {
            double sum = 0;
            foreach (var variable in VariableList)
            {
                sum += variable.Variable.Weight;
            }
            SumOfWeights = sum;
        }

  
}
}
