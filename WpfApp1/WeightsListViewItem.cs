﻿using PorzadkowanieLiniowe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.ViewModels;

namespace WpfApp1
{
    public class WeightsListViewItem :BaseViewModel
    {

        public WeightsListViewItem(string t, string v, Variable variable)
        {
            Variable = variable;
            this.VariableName = t;
            this.VariableWeight = v;
        }
        private string _variableWeight;
        private string _variableName;
 
        private Variable _variable;
        public string VariableName
        {
            get => _variableName;
            set => SetProperty(ref _variableName, value);
        }

        public string VariableWeight
        {
            get => _variableWeight;
            set => SetProperty(ref _variableWeight, value);
        }
        public Variable Variable { get => _variable; set => _variable = value; }

       
    }

}
