﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using PorzadkowanieLiniowe;
using WpfApp1;
using WpfApp1.ViewModels;

namespace GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string path;
        public DataTable data;
        public DataSeries dataSeries;
        public List<BaseViewModel> ViewModels { get; set; } = new List<BaseViewModel>();
        public int CurrentViewModel { get; set; }
        public LinearOrderingMethod linearOrderingMethod { get; set; } = new LinearOrderingMethod();
        DataTableViewModel dataTableViewModel;
        public MainWindow()
        {
            InitializeComponent();
            introductionText.Text = "Welcome to Linear Ordering Tool.\n\nIt is an application which will help you with determining the best objects out of the provided ones thanks to some mathematical and statistical methods as well as visual graphs.\n" +
                "Please provide .csv file with proper data to proceed.";
        }

        private void OpenFile_Clicked(object sender, RoutedEventArgs e)
        {
            path = null;
            NextViewModelBt.Visibility = Visibility.Hidden;
            PreviousViewModelBt.Visibility = Visibility.Hidden;
            DataContext = null;
            ContentDataGrid.DataContext = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
                path = openFileDialog.FileName;
            
            try
            {
                ViewModels.Clear();
                data = DataFromCSVFile.ImportDataFromCSVFile(path);
                dataSeries = new DataSeries();
                dataSeries.GenerateDataSeries(data);
                dataTableViewModel = new DataTableViewModel(data);
                SpiderChartViewModel spider = new SpiderChartViewModel(linearOrderingMethod,dataSeries, dataTableViewModel, data);
                ViewModels.Add(new VariablesIsStymuViewModel(dataSeries));
                ViewModels.Add(new LinearMethodViewModel(spider));
                ViewModels.Add(new WeightsSelectionViewModel(dataSeries));
                ViewModels.Add(spider);
                AnalizeBt.Visibility = Visibility.Visible;
                CurrentViewModel = 0;
                ContentDataGrid.DataContext = dataTableViewModel;
                introLabel.Visibility = Visibility.Hidden;
                podiumImg.Visibility = Visibility.Hidden;
                chartImg.Visibility = Visibility.Hidden;
            }
            catch (ArgumentNullException)
            {
                AnalizeBt.Visibility = Visibility.Hidden;
                introLabel.Visibility = Visibility.Visible;
                podiumImg.Visibility = Visibility.Visible;
                chartImg.Visibility = Visibility.Visible;
            }
            
                
        }

        private void AnalizeBt_Clicked(object sender, RoutedEventArgs e)
        {
            DataContext = ViewModels[0];
            AnalizeBt.Visibility = Visibility.Hidden;
            NextViewModelBt.Visibility = Visibility.Visible;
            PreviousViewModelBt.Visibility = Visibility.Visible;
            NextViewModelBt.IsEnabled = true;
            PreviousViewModelBt.IsEnabled = false;
        }

        private void NextViewModelBt_Clicked(object sender, RoutedEventArgs e)
        {
              CurrentViewModel += 1;
              ButtonsEnabled(CurrentViewModel);
              DataContext = ViewModels[CurrentViewModel];
        }
        public void PreviousViewModelBt_Clicked(object sender, RoutedEventArgs e)
        {
              CurrentViewModel -= 1;
              ButtonsEnabled(CurrentViewModel);
              DataContext = ViewModels[CurrentViewModel];
        }
        public void ButtonsEnabled(int currentview)
        {
            if(currentview>0 && currentview<ViewModels.Count-1)
            {
                PreviousViewModelBt.IsEnabled = true;
                NextViewModelBt.IsEnabled = true;
            }
            else if (currentview <= 0)
            {
                PreviousViewModelBt.IsEnabled = false;
                NextViewModelBt.IsEnabled = true;
            }
            else if (currentview >= ViewModels.Count()-1)
            {
                PreviousViewModelBt.IsEnabled = true;
                NextViewModelBt.IsEnabled = false;
            }
        }
    
    }
}
