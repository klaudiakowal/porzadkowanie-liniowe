﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1
{
    public enum LinearOrderingMethodName
    {
        Hellwig,
        TOPSIS,
        StandarizedSum,
        Rank,
        Unknown
    }

    public enum NormalizationMethodName
    {
        Standarization,
        PostitionalStandarization,
        Unitarization,
        Unknown
    }


    public class LinearOrderingMethod
    {
        public LinearOrderingMethod()
        {
            LinOrdMethod = LinearOrderingMethodName.Unknown;
            NormMethod = NormalizationMethodName.Unknown;
        }

        public LinearOrderingMethod(LinearOrderingMethodName linOrdMethod, NormalizationMethodName normMethod)
        {
            LinOrdMethod = linOrdMethod;
            NormMethod = normMethod;
        }

        public LinearOrderingMethodName LinOrdMethod { get; set; }

        public NormalizationMethodName NormMethod { get; set; }
        
    }
}
