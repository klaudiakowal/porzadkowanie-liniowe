﻿using PorzadkowanieLiniowe;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.ViewModels;

namespace WpfApp1
{
    public class VariableIsStymulantCheckBoxListViewItem : BaseViewModel
    {
        private bool _isChecked;
        private string _variableName;
        public bool IsChecked
        {
            get => _isChecked;
            set
            {
                SetProperty(ref _isChecked, value);
                Variable.IsStimulant = value;
            }
        }
        private Variable _variable;
        public string VariableName
        {
            get => _variableName;
            set => SetProperty(ref _variableName, value);
        }
        public Variable Variable { get => _variable; set => _variable = value; }

        public VariableIsStymulantCheckBoxListViewItem(string t, bool c, Variable variable)
        {
            Variable = variable;
            this.VariableName = t;
            this.IsChecked = c; 
        }
    }
}
