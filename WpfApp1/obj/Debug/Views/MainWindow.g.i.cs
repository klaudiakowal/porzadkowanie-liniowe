﻿#pragma checksum "..\..\..\Views\MainWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "64FE2A096EF030B6B5D9C1007BA7D610159331B9"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using GUI;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfApp1.ViewModels;
using WpfApp1.Views;


namespace GUI {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 45 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button OpenFileButton;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ContentControl ContentDataGrid;
        
        #line default
        #line hidden
        
        /// <summary>
        /// NextViewModelBt Name Field
        /// </summary>
        
        #line 50 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Button NextViewModelBt;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button PreviousViewModelBt;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AnalizeBt;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label introLabel;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock introductionText;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image podiumImg;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\Views\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image chartImg;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WpfApp1;component/views/mainwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\MainWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.OpenFileButton = ((System.Windows.Controls.Button)(target));
            
            #line 45 "..\..\..\Views\MainWindow.xaml"
            this.OpenFileButton.Click += new System.Windows.RoutedEventHandler(this.OpenFile_Clicked);
            
            #line default
            #line hidden
            return;
            case 2:
            this.ContentDataGrid = ((System.Windows.Controls.ContentControl)(target));
            return;
            case 3:
            this.NextViewModelBt = ((System.Windows.Controls.Button)(target));
            
            #line 50 "..\..\..\Views\MainWindow.xaml"
            this.NextViewModelBt.Click += new System.Windows.RoutedEventHandler(this.NextViewModelBt_Clicked);
            
            #line default
            #line hidden
            return;
            case 4:
            this.PreviousViewModelBt = ((System.Windows.Controls.Button)(target));
            
            #line 51 "..\..\..\Views\MainWindow.xaml"
            this.PreviousViewModelBt.Click += new System.Windows.RoutedEventHandler(this.PreviousViewModelBt_Clicked);
            
            #line default
            #line hidden
            return;
            case 5:
            this.AnalizeBt = ((System.Windows.Controls.Button)(target));
            
            #line 52 "..\..\..\Views\MainWindow.xaml"
            this.AnalizeBt.Click += new System.Windows.RoutedEventHandler(this.AnalizeBt_Clicked);
            
            #line default
            #line hidden
            return;
            case 6:
            this.introLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.introductionText = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.podiumImg = ((System.Windows.Controls.Image)(target));
            return;
            case 9:
            this.chartImg = ((System.Windows.Controls.Image)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

